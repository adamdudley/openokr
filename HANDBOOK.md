# Introduction

The purpose of this handbook is to maximize our efficiency. We will model GitLab in how it values efficiency and this handbook and the practice of "handbook first" will be a step into this value. Please use a merge request to suggest improvements or revisions and use issues for questions.

# Organization

## Values

- **Efficiency** because as far as any of us knows this is the only time we have.
- **Beauty** because it's a worthy ideal to strive for.
- **Collaboration** because we can do more impactful things in better ways when we work together.
- **Kindness**
- **Awareness**
- **Agency**
- **Learning**

## Mission

To create technology that results in happier and healthier human beings.

## Practices

Our practices ensure we are executing from our values.

### Write it down

If you learned it and others on the team could benefit from knowing it, write it down and add it to the handbook after you've refined it.

# Engineering

## Conventions

### .md files

Write all .md files using Markdown because it's efficient and beautiful.

### Skinny To-do Lists

It's easy for To-do Lists to get out of hand, so I propose a Skinny To-do List rule.

Just as it is prescribed to have Skinny controllers in Ruby/Rails and Skinny components in React, I am proposing we prescribe Skinny To-do Lists so we stay sane! 😄

Until the project gets larger, stick to creating our to-dos under major category lists such as API, UI, Frontend, Backend, DB, Docs, DevOps, etc. (Some of these are already in place.)

Later, as the project grows, we can add major subcategories as needed.
