class RemoveUserIdFromKeyResults < ActiveRecord::Migration[6.0]
  def change
    remove_column :key_results, :user_id
  end
end
