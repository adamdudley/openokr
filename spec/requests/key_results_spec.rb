require 'rails_helper'

RSpec.describe 'Key Results API' do
  let(:user) { create(:user) }
  let!(:objective) { create(:objective, user_id: user.id) }
  let!(:key_results) { create_list(:key_result, 20, objective_id: objective.id) }
  let(:objective_id) { objective.id }
  # let(:user_id) { user.id }
  let(:key_result_id) { key_results.first.id }
  let(:headers) { valid_headers }

  describe '/objectives/:objective_id/key_results' do
    before { get "/objectives/#{objective_id}/key_results", params: {}, headers: headers }

    context 'when objective exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all objective key_results' do
        expect(json.size).to eq(20)
      end
    end

    context 'when objective does not exist' do
      let(:objective_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Objective/)
      end
    end

  end

  describe 'GET /objectives/:objective_id/key_results/:id' do
    before { get "/objectives/#{objective_id}/key_results/#{key_result_id}", params: {}, headers: headers }

    context 'when the record exists' do
      it 'returns the key result' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(key_result_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:key_result_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find KeyResult/)
      end
    end
  end

  describe 'POST /objectives/:objective_id/key_results' do
    let(:valid_attributes) { { description: 'Make 10 more sales appointments', due_date: 'Fri, 29 Nov 2019' }.to_json }

    context 'when request attributes are valid' do
      before do
        post "/objectives/#{objective_id}/key_results", params: valid_attributes, headers: headers
      end
      
      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when an invalid request' do
      before { post "/objectives/#{objective_id}/key_results", params: {}, headers: headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a failure message' do
        expect(response.body).to match(/Validation failed: Description can't be blank, Due date can't be blank/)
      end
    end
  end

  describe 'PUT /objectives/:objective_id/key_results/:id' do
    let(:valid_attributes) { { description: 'Make 11 more sales appointments' }.to_json }

    context 'when record exists' do
      before { put "/objectives/#{objective_id}/key_results/#{key_result_id}", params: valid_attributes, headers: headers }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  describe 'DELETE /objectives/:objective_id/key_results/:id' do
    before { delete "/objectives/#{objective_id}/key_results/#{key_result_id}", params: {}, headers: headers }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end

end
