require 'faker'

FactoryBot.define do
  factory :key_result do
    description { Faker::Lorem.word }
    due_date { Faker::Date }
    objective
    end
  end
