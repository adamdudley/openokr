require 'faker'

FactoryBot.define do
  factory :objective do
    description { Faker::Lorem.word }
    score { Faker::Number.between(from: 0.1, to: 1.0) }
    user
  end
end