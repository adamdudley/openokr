# Welcome from Adam Dudley, founder of OpenOKR

Thank you for taking the time to contribute to OpenOKR! I'm excited you're as enthusiastic about OKRs as I am. It is my belief that human beings are happier and healthier when they're setting, striving for and achieving objectives that are meaninful to them. Let's learn and work together to build a great tool for all humanity to use and benefit from. 👍🏼😊

Check back because I'll be building out the following sections in this document over time.

## Product Vision

My vision for OpenOKR is that it becomes a robust, performant, and maintainable open source web and mobile application and a serious tool for tracking personal and professional OKRs individually or with a team. For the future, I also envision a community element where we can publicly share and track our OKRs and support each other with resources and encouragement in achieving them.

## Product Mission

To create technology that results in happier and healthier human beings.

## Quick start

First, clone the OpenOKR repository at https://gitlab.com/adamdudley/openokr.

If this is your first time cloning a repo, check out this section in the GitLab Docs: https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository.

Second, create and check out a new branch so you can commit your changes and push your branch, which will create a GitLab merge request. (ALWAYS use a new branch!!!) Then, in time, your contribution will be reviewed by the Code Review Committee and approved if there are no issues.

If this is your first time doing any of these git operations, check out this section in the GitLab Docs: https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#new-merge-request-from-you-local-environment.

### Why read these guidelines

They'll help you get oriented and make the most of your contributions.

### Here's what I'm NOT looking for

Assholes with big egos who can't get over themselves or take direct feedback.

### Here's what I AM looking for

Adults (age has nothing to do with it) who want to contribute and learn and who can take direct feedback.

### Why read these guidelines

## >Ground rules

### Your first contribution

### How to submit a contribution

### Small fixes

## How to report a bug

### How to file a bug report

## How to suggest a feature or enhancement

### How to suggest a feature

Here at OpenOKR we have embraced the Shape Up methodology for shipping software that is described in an online book written by the folks at Basecamp. Here it that book: https://basecamp.com/shapeup.

In order to suggest a feature for OpenOKR you'll need to write and submit a pitch that follows the Shape Up format as described in this section of the book: https://basecamp.com/shapeup/1.5-chapter-06.

Please write up your feature suggestion using Markdown and submit it as a new issue here: https://gitlab.com/adamdudley/openokr/issues/new. In time, your submission will be reviwed and bet on by a committee made up of senior folks on the project.

## Code review process

### How a contribution gets accepted

## Code, commit message, and labeling conversations
