# OpenOKR

## Table of contents

- [General info](#general-info)
- [Technologies](#technologies)
- [Features](#features)
- [Status](#status)
- [Contributing](#contributing)
- [Contact](#contact)

## General info and Puprose

- This is an open source application called OpenOKR for anyone who wants to create and track personal or professional Objectives & Key Results (OKRs).

- OKRs are a neat planning and productivity model used by numerous big names in tech like GitLab, Google, Twitter, Spotify, Netflix, Amazon, Facebook, Intel (ground-zero for OKRs!), etc.

- You can learn more about OKRs at the following three links:

  - https://assets.ctfassets.net/cn6v7tcah9c0/4snZXJ821G6KYUoc08masK/58ffcbc7c607d7c6056c7da507727135/Google_OKR_Playbook_V1JS.pdf

  - https://www.whatmatters.com/

  - https://blog.weekdone.com/what-companies-use-okrs/

## Technologies

### Backend

- PostgreSQL (v12.1)

### Middle-tier

- Ruby (v2.6.5) on Rails (v6.0.1) for the API
- RSpec test suite

### Frontend

- React (v16.12.0) (creat-react-app) for the client and UI
  - React-Dom, React-Router-Dom, React-Router
  - Redux (v4.05), React-redux, Redux-thunk
  - React-bootstrap
- Jest test suite with Enzyme

## Setup

- Run Rails backend on port 4000 `rails s --p 4000`
- Run React frontend on other port with `npm start`

## Rails Middle-tier Features

- Rails is used for the API, which is set up for versioning
- JSONWebToken is used for user authentication and route protection
- Active Record is used for defining object associations and data manipulation with the PostgreSQL database
- RSpec is used for testing

## React Frontend Features

- Redux is used to manage state. Check documentation regarding the new features that work with React hooks.
- React hooks are used to manage local state.
- Fetch API used to perform CRUD actions with the Rails middle-tier.
- Jest and enzyme are used for testing

## Contributing

https://gitlab.com/adamdudley/openokr/blob/master/CONTRIBUTING.md

## Status

In development with target MVP deployment late January/early Februrary 2020

<!-- ## Inspiration -->

https://gitlab.com/adamdudley/openokr/blob/master/PITCH.md

## Contact

Adam Dudley | dudleyadam@gmail.com
