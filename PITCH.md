# Pitch for OpenOKR MVP or v1.0.0 (12/23/19)

## From [this section](https://basecamp.com/shapeup/1.5-chapter-06) in Basecamp's Shape Up:

The purpose of the pitch is to present a good potential bet. It’s basically a presentation. The ingredients are all the things that we need to both capture the work done so far and present it in a form that will enable the people who schedule projects to make an informed bet.

There are five ingredients that we always want to include in a pitch:

- **Problem** — The raw idea, a use case, or something we’ve seen that motivates us to work on this
- **Appetite** — How much time we want to spend and how that constrains the solution
- **Solution** — The core elements we came up with, presented in a form that’s easy for people to immediately understand
- **Rabbit holes** — Details about the solution worth calling out to avoid problems
- **No-gos** — Anything specifically excluded from the concept: functionality or use cases we intentionally aren’t covering to fit the appetite or make the problem tractable

## The Pitch

### Problem

A good portion of my career prior to pivoting into programming was spent coaching entrepreneurs, solopreneuers, and small business owners. What I think I learned about human beings from this experience is that we naturally like to be productive and make tangible progress on things that matter to us.

Objectives & Key Results (OKRs) is a model that adds clarity, specificity, measurability, and accountability to our human endeavors in a way that makes us more efficient in achieving them.

A Google search will reveal that there are a number of tools out there ranging from free and simple to expensive and sophisticated for tracking OKRs. I haven't looked at any of these because I don't really care what anyone else has done already. I just want to build a great product in collaboration with other developers and its users and following a combination of the Lean Startup and Basecamp's Shape Up approaches.

### Appetite

I think a solid MVP can be put together and deployed within a 6-week "big batch" cycle putting us somewhere in the neighborhood of late January or early February 2020.

### Solution

The MVP for OpenOKR is simple. Here is all it needs to do:

1. Allows a user to sign up and log in
2. Allows a user to recover their password
3. Allows a user to view all of their objectives and key results in a reasonably elegant way
4. Allows a user to add an objective and a score (0.1 - 1.0)
5. Allows a user to add key results and a due date for each to an objective
6. Allows a user to mark a key result as completed/delivered
7. ~~Allows a user to archive an objective that's no longer being worked on~~
8. Allows a user to edit their objectives and key results
9. Allows a user to delete their objectives and key results
10. ~~Allows a user to easily rearrange/reorder their objectives and key results in the view~~

### Rabbit holes

### No gos

Let's keep this MVP simple and do the very minimum necessary to bring in some users to tell us what's wrong or missing and what they want to see made different!!! Complexity will be the enemy of DONE here. Look at how I've struck through several of the MVP requirements above. This is the stuff that's unnecessary for MVP because our users might not even care about these features. Let's let them tell us what they want and what's important to them rather than build something without knowing in advance if it's going to be useful or meaningful.
