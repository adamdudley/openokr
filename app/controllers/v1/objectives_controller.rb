module V1
  class ObjectivesController < ApplicationController
      before_action :set_objective, only: [:show, :update, :destroy]

    def index
      @objectives = current_user.objectives.paginate(page: params[:page], per_page: 20)
      json_response(@objectives)
    end

    def create
      @objective = current_user.objectives.create!(objectives_params)
      json_response(@objective, :created)
    end

    def show
      json_response(@objective)
    end

    def update
      @objective.update(objectives_params)
      head :no_content
      # Not sure about using the below this from a security standpoint ... see [this](https://stackoverflow.com/questions/14716151/why-does-rails-want-to-return-head-no-content-for-json-put-requests) and [this](https://github.com/rails/rails/issues/1742)
      # json_response(@objective)
    end

    def destroy
      @objective.destroy
      head :no_content
    end

  private

    def objectives_params
      params.require(:objective).permit(:description, :score, :user_id, :objective_id)
    end

    def set_objective
      @objective = Objective.find(params[:id])
    end

  end
end