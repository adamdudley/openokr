module V1
  class KeyResultsController < ApplicationController
    before_action :set_objective
    before_action :set_objective_key_result, only: [:show, :update, :destroy]

    def index
      json_response(@objective.key_results)
    end

    def create
      @key_result = @objective.key_results.create!(key_results_params)
      json_response(@key_result, :created)
    end

    def show
      json_response(@key_result)
    end

    def update
      @key_result.update(key_results_params)
      head :no_content
    end

    def destroy
      @key_result.destroy
      head :no_content
    end

  private

    def key_results_params
      # Tests will not pass with .require(:key_result)
      # Seems to be something with regards to nested resources:
      # https://stackoverflow.com/questions/39046532/unpermitted-parameter-in-rails-5
      params.permit(:description, :due_date, :objective_id)
    end

    def set_objective
      @objective = Objective.find(params[:objective_id])
    end

    def set_objective_key_result
      @key_result = @objective.key_results.find_by!(id: params[:id]) if @objective
    end

  end
end