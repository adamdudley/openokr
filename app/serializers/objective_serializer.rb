class ObjectiveSerializer < ActiveModel::Serializer
  attributes :id, :description, :score, :user_id
  has_many :key_results
end
