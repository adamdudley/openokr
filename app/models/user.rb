class User < ApplicationRecord

  has_secure_password

  has_many :objectives,
    dependent: :destroy
  validates_associated :objectives

  validates :name,
    presence: true
  
  EMAIL_FORMAT = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  
  validates :email,
    presence: true,
    uniqueness: true,
    format: EMAIL_FORMAT
  
  PASSWORD_FORMAT = /\A(?=.{8,64})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[[:^alnum:]])/x   
  
  validates :password,
    presence: true,
    format: PASSWORD_FORMAT
  validates :password_digest,
    presence: true

end