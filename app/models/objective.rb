class Objective < ApplicationRecord
  
  belongs_to :user
  # TODO: Add ability to reassign objectives and key results with teams feature
  has_many :key_results,
    dependent: :destroy
  validates_associated :key_results

  validates :description,
    presence: true,
    length: { maximum: 280 }

  validates :score,
    presence: true,
    numericality: true,
    inclusion: { in: 0.1..1.0, message: "Score value must be between 0.1 and 1.0" }

end
