class KeyResult < ApplicationRecord

  belongs_to :objective

  validates :description,
    presence: true,
    length: { maximum: 280 }
  validates :due_date,
    presence: true

end
